var fs=require("fs")
var _=require("underscore")
var modules_dir=__dirname+"/app_modules";

var loadAppContextData=function(callback){
	fs.readFile(__dirname+"/app_context.json", function(err, data){
		if(err==null){
			callback(JSON.parse(data.toString()));
		}
	})
}

var listModules=function(callback){
	fs.readdir(modules_dir, function(err, dir_list){
		var async=require("async")
		var parllel_checks=_.map(dir_list, function(item){
			return function(callback){
				fs.stat(modules_dir+"/"+item, function(err, stat){
					if(stat.isDirectory()){
						callback(null,item)
					}
					else{
						callback(null,null)
					}
				});
			}
		});
		async.parallel(
			parllel_checks	
			,function(err, results){
				results=_.filter(results, function(item){
					if(item!=null)
						return item;
				})
				callback(results)
			});
	});
}

var loadModules=function(appContext,callback){
	listModules(function(moduleList){
		_.each(moduleList,function(moduleName){
			console.log("Loading module::"+moduleName);
			var module=require(modules_dir+"/"+moduleName);
			appContext.loadModule(module)
		})
		callback();
	})
}

var init=function(){
	loadAppContextData(function(context_data){
		var AppContext=require('./appContext.js')
		appContext=new AppContext(context_data);
		loadModules(appContext, function(){
			console.log("All modules Loaded")
			startModules(appContext, console.log)
		});
	});
}

var startModules=function(appContext,callback){
	for(moduleName in appContext.modules){
		appContext.startModule(appContext.modules[moduleName]);
	}
	callback("All modules started");
}

init();

GLOBAL._=require('underscore')