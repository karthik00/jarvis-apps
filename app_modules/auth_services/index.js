exports.name="auth_services";

exports.onLoad=function(context){
	
}

exports.onStart=function(context){
	configureRoutes(this, context);
}

exports.onStop=function(context){
	context.removeAllRoutes(this);
}

var configureRoutes=function(module,context){
	var app=require("./app.js")
	context.addRoute(module, "/", 'post', app.authenticationHttpHandler);
}