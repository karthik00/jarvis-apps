var QUEUE_PORT=process.env.JARVIS_JOBQUEUE_PORT;
var QUEUE_IP=process.env.JARVIS_EXTERNAL_IP;
exports.name="beanstalkd";
var spawn = require('child_process').spawn;
var beanstalkd = null;

exports.onLoad=function(context){
	
}

exports.onStart=function(context){
	beanstalkd=spawn('beanstalkd', ['-l', QUEUE_IP,'-p',QUEUE_PORT]);
	beanstalkd.stdin.end();
}

exports.onStop=function(context){
	if(beanstalkd!=null){
		beanstalkd.kill('SIGHUP');
	}
}