exports.name="sys_health";
var os = require("os")

exports.onLoad=function(context){
	
}

exports.onStart=function(context){
	configureRoutes(this, context);
}

exports.onStop=function(context){
	context.removeAllRoutes(this);
}

var configureRoutes=function(module,context){
	context.addRoute(module, "/", 'post', fetchSysHealth);
}


var fetchSysHealth= function(req, res){
	var sys_data={
		host_name	: 	os.hostname(),
		os_type		: 	os.type(),
		platform	: 	os.platform(),
		arch		: os.arch(),
		release		: os.release(), 
		uptime		: os.uptime(),
		avg_load	:os.loadavg(),
		total_mem	:os.totalmem(),
		free_mem	:os.freemem(),
		cpus		:os.cpus()
	}
	res.send(sys_data);
}
