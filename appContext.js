var HTTP_PORT=process.env.JARVIS_HTTP_ALT1_PORT;

var appContext={
	modules:{}
};

var nowjs_everyone=null;
var express = require('express');
var app=express();

app.configure(function(){
	var expressLayouts=require('express-ejs-layouts');
	var store = new express.session.MemoryStore;
	var path = require('path')
	app.use(express.cookieParser());
	app.set('port', HTTP_PORT || 3000);
	app.use(express.session({ secret: 'kjhkjh54597988asd68aksjhd8as7i', store: store }));
    app.set('layout', 'layout')
    app.set('views', __dirname + '/views');
    app.set('view engine', 'ejs');
    app.use(expressLayouts)
    app.use(express.favicon());
    app.use(express.logger('dev'));
    app.use(express.bodyParser());
    app.use(express.methodOverride());
    app.use(app.router);
    app.use(express.static(path.join(__dirname, 'public')));
});

module.exports=function(context_data){
	appContext.applicationName=context_data.applicationName;
	appContext.startHttpService(appContext);
	return appContext;
}

appContext.addRoute=function(module, route, method, handler){
	if(method=='get'){
		app.get("/"+module.name+"/"+route, handler);
	}
	else if(method='post'){
		app.post("/"+module.name+route, handler);
	}
}

appContext.removeAllRoutes=function(module){
	var _=require("underscore")
	app.routes.get=_.filter(app.routes.get, function(route, index){
		return route.path.indexOf("/"+module.name+"/") != 0;
	});
	app.routes.post=_.filter(app.routes.post, function(route, index){
		return route.path.indexOf("/"+module.name+"/") != 0;
	});
	console.log(app.routes)
}


appContext.startModule=function(module){
	module.status=this.moduleStatus.STARTED;
	module.onStart(this);
	if(typeof everyone.now.moduleStarted == 'function')
		everyone.now.moduleStarted(module.name);
}

appContext.stopModule=function(module){
	module.status=this.moduleStatus.STOPPED;
	module.onStop(this);
	if(typeof everyone.now.moduleStopped == 'function')
		everyone.now.moduleStopped(module.name);
}

appContext.loadModule=function(module){
	this.modules[module.name]=module;
	module.onLoad(this);
}

appContext.startHttpService=function(appContext){
	configureRoutes(this);
	console.log("Server started on "+HTTP_PORT+" port")
	var server=app.listen(HTTP_PORT || 3000);
	everyone = require("now").initialize(server);
	initilize_everyone(everyone, appContext);
}

var initilize_everyone=function(everyone, appContext){
	everyone.now.startModule=function(module_name){
		appContext.startModule(appContext.modules[module_name]);
	}
	everyone.now.stopModule=function(module_name){
		appContext.stopModule(appContext.modules[module_name]);
	}
}

var configureRoutes=function(context){
	app.get('/', function(req, res){
		res.render('index', {context:context});
	});
}

appContext.moduleStatus={
	STARTED:"started",
	STOPPED:"stopped"
}